﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Imaging;

using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB.Events;
using Autodesk.Revit.UI.Events;

public class AuthoringUpdate : IExternalApplication
{
    public void GetDoc(object sender, DocumentOpenedEventArgs args)
    {
        //get document from the event args
        Document doc = args.Document;

        Transaction tr = new Transaction(doc, "Testing");
        if (tr.Start() == TransactionStatus.Started)
        {
            doc.ProjectInformation.Author = "other tests";
            tr.Commit();
        }
    }

    public Result OnStartup(UIControlledApplication app)
    {
        //register the event
        try
        {
            app.ControlledApplication.DocumentOpened += new EventHandler<Autodesk.Revit.DB.Events.DocumentOpenedEventArgs>(GetDoc);
        }

        catch (Exception)
        {
            return Autodesk.Revit.UI.Result.Failed;
        }

        return Result.Succeeded;
    }

    public Result OnShutdown(UIControlledApplication app)
    {
        //remove event
        app.ControlledApplication.DocumentOpened -= GetDoc;
        return Result.Succeeded;
    }
}

public class WorksetDialogTest : IExternalApplication
{
    // Eventhandler which responds when the workset dialog box appears
    public void WorksetDialogBoxHandler(object sender,DocumentOpenedEventArgs docargs, TaskDialogShowingEventArgs args)
    {
        Type dboxtype = args.GetType();
        Document doc = docargs.Document;
        

        using (Transaction tr = new Transaction(doc, "Starting..."))
        {
            if(tr.Start() == TransactionStatus.Started)
            {
                //if(dboxtype.Equals())

                tr.Commit();
            }

        }
    }

    public Result OnStartup(UIControlledApplication app)
    {
        return Result.Succeeded;
    }

    public Result OnShutdown(UIControlledApplication app)
    {
        return Result.Succeeded;
    }

}

