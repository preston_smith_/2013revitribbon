﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Imaging;

using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Events;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;

[TransactionAttribute(TransactionMode.Manual)]
[RegenerationAttribute(RegenerationOption.Manual)]

//Panel which will hold all BR+A Stuff
public class RibbonTest : IExternalApplication
{
    public Result OnStartup(UIControlledApplication ui_app)
    {
        // Creates a new ribbon tab
        string tabname = "Debugging";
        ui_app.CreateRibbonTab(tabname);

        // Creates the push button to add to the tab
        PushButtonData tb1_data = new PushButtonData("The Testing|Debug Area",
                                                        "DebugArea",
                                                        @"C:\ProgramData\Autodesk\Revit\Addins\2014\TestingButton.dll",
                                                        "UnitTestArea");

        // Creates a ribbon panel and add button to it
        RibbonPanel newpanel = ui_app.CreateRibbonPanel(tabname, "TestPanel");
        newpanel.AddItem(tb1_data);
        
        return Result.Succeeded;
    }

    public Result OnShutdown(UIControlledApplication ui_app)
    {
        return Result.Succeeded;
    }
}