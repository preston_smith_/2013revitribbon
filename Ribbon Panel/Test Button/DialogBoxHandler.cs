﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Imaging;

using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB.Events;
using Autodesk.Revit.UI.Events;

public class DialogBoxHandler : IExternalApplication
{
    public UIControlledApplication uiapp;
    public UIApplication app;

    public void AutoCloseDialog(object sender, 
        DialogBoxShowingEventArgs tdargs)
    {
        //TaskDialog td = new TaskDialog("Warning");
        //td.MainContent =
        //    "A dialog box is about to appear, if you'd like to prevent this, click OK";
        //TaskDialogCommonButtons buttons = TaskDialogCommonButtons.Cancel | TaskDialogCommonButtons.Ok;
        //td.CommonButtons = buttons;
        
        //app.DialogBoxShowing -= AutoCloseDialog;

        //TaskDialogResult choice = td.Show();

        //if (TaskDialogResult.Cancel == choice)
        //{
        //    tdargs.OverrideResult(0);
        //}

        //else
        //{
        //    tdargs.OverrideResult(1);
        //}

        //app.DialogBoxShowing +=
        //    AutoCloseDialog;

        Document doc = app.ActiveUIDocument.Document;
        Transaction tr = new Transaction(doc, "Testing");
        if (tr.Start() == TransactionStatus.Started)
        {
            doc.ProjectInformation.Author = "The event handler worked";
            tr.Commit();
        }
    }

    public Result OnStartup(UIControlledApplication app)
    {
        //register the event
        try
        {
            app.DialogBoxShowing +=
                new EventHandler<DialogBoxShowingEventArgs>(AutoCloseDialog);
        }

        catch (Exception)
        {
            return Result.Failed;
        }

        return Result.Succeeded;
    }

    public Result OnShutdown(UIControlledApplication app)
    {
        app.DialogBoxShowing -=
            AutoCloseDialog;

        return Result.Succeeded;
    }
}
